<div class="upload col-md-4">

    <h4>UploadPhoto V 1.0</h4>
    <div>
        <form enctype="multipart/form-data" method="post" action="/plugin.php">
            <div class="form-group"><label for="file">Выберете фото: </label>
                <input type="file" name="picture">
            </div>
            <div class="form-group"><label for="">Укажите роль: </label><br>
                <select name="type_photo" id="">
                    <option value="1">Основное</option>
                    <option value="2">Превью</option>
                    <option value="3">Дополнительное</option>
                </select>
            </div>
            <div>
                <label for="">Описание:<br>
                    <textarea name="about" id="about" cols="30" rows="4"></textarea>
                </label>
            </div>
            <div>
                <? $routes = explode('/', $_SERVER['REQUEST_URI']);

                                    // получаем имя контроллера
                                    if ( !empty($routes[1]) )
                                    {
                                    $controller_name = $routes[1];
                                    }
                                    else $controller_name = "main";
                                    //или присваиваем значение по умолчанию
                                    ?>
                <input type="text" name="modul" value="<? echo $controller_name ?>" style="display: none">
            </div>
            <div class="form-group"><button class="btn btn-info" name="but" value="up" type="submit">Загрузить</button></div>
            <hr>
            <div><h5>Или выберите удаление фото из модуля</h5></div>
            <div class="form-group"><button class="btn btn-danger" name="but" value="del" type="submit">Удалить</button></div>
        </form>
    </div>
</div>