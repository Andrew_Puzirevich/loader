<?php

class Controller_Page extends Controller
{


    function __construct()
    {
        $this->model = new Model_Page();
        $this->view = new View();
    }

    function action_index()
	{
	    $data = $this->model->get_data();
		$this->view->generate('page_view.php', 'template_view.php', $data);
	}
}
