<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>NewCMS</title>
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
		<link rel="stylesheet" href="/bootstrap/css/bootstrap.css" type="text/css" />

	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="menu">
					<ul>
						<li><a href="/">Главная</a></li>
						<li><a href="/page">Страница</a></li>
						<li><a href="/portfolio">Портфолио</a></li>
						<li><a href="/contacts">Контакты</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">

				<div id="content" class="row">

                    <div>
                        <?php include '/form_plugin.php';?>
                    </div>

					<div class="box col-md-7">
						<?php include 'application/views/'.$content_view; ?>

					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>

		</div>
		<div id="footer">
			<a>A.P. 2017</a>
		</div>
	</body>
</html>