<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 09.04.2017
 * Time: 14:47
 */
//конект к базе
require_once('application/core/connect.php');
//обработка post запроса для загрузки файла

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $modul = $_POST['modul'];
    $action = $_POST['but'];
    switch ($action) {
        case 'up':
            //пути, сохранение и временная папка
            $path = 'images/';
            $name = $path . basename($_FILES['picture']['name']);
            // Массив допустимых значений типа файла
            $types = array('image/gif', 'image/png', 'image/jpeg');
            // Максимальный размер файла
            $size = 1024000;
            //получаем значения
            $url = $path . $_FILES['picture']['name'];
            $type = $_POST['type_photo'];
            $text = $_POST['about'];
            // Проверяем тип файла
            if (!in_array($_FILES['picture']['type'], $types))
                die('<p>Недопустимый тип файла. <a href="/' . $modul . '">Попробовать другой файл?</a></p>');
            // Проверяем размер файла
            if ($_FILES['picture']['size'] > $size)
                die('<p>Слишком большой размер файла. <a href="/' . $modul . '">Попробовать другой файл?</a></p>');
            //Перемещаем из временной директории в нашу папку
            if (!move_uploaded_file($_FILES["picture"]["tmp_name"], $name))
                echo '<p>Загрузка не произошла</p>';
            else
                echo '<p>Загрузка прошла удачно.</p>';
            // если все хорошо, запишем таблицу с картинкой
            mysqli_query($link, "INSERT INTO Picture (url) VALUE ('$url')");
            // теперь выведем картинку, с назначением
            //получим последний id для таблицы модели
            $query = mysqli_fetch_row(mysqli_query($link, "SELECT MAX(id) FROM Picture"));
            $max_id = $query[0];
            //выводим картинку
            echo('<p>Для продолжения нажмите кнопку Сохранить</p><br><img src="' . $url . '" alt="" width="300px" style="margin-bottom: 25px;"><br>');
            //пишем ее в таблицу модели
            $req = "INSERT INTO {$modul} (name_p,text,id_pic,role) VALUE ('$name', '$text','$max_id','$type')";
            mysqli_query($link, $req);
            //отправляем пользователя на страницу модуля
            echo('<a href="/' . $modul . '" style="background: rgba(96, 125, 139, 0.75); border-radius: 5px; padding: 10px;">Сохранить</a>');
            break;
        case 'del':
            $fordel = (mysqli_query($link, "SELECT * FROM {$modul}"));
            foreach ($fordel as $value) {
                if ($value["role"] == 1) {
                    $role = "основного фото модуля";
                } elseif ($value["role"] == 2) {
                    $role = "превью фото модуля";
                } else $role = "дополнительного фото модуля";
                echo
                ('
                        <form method="post" action="' . $_SERVER['PHP_SELF'] . '">
                        <div>
                            <p>Название ' . $role . ': ' . $value["name_p"] . '</p><br>
                            <img src="' . $value["name_p"] . '" width="100px"><br>
                            <p>Описание:<br>' . $value["text"] . '</p><br>
                            <input type="text" style="display:none;" name="id_pic" value="' . $value["id_pic"] . '">
                            <input type="text" style="display:none;" name="modul" value="' . $modul . '">
                            <input type="text" style="display:none;" name="name_pic" value="' . $value["name_p"] . '">
                            <button type="submit" name="del_pic">Удалить</button>
                        </div>
                        </form>
                    ');
                //обработчик удаления с возвращением в плагин

            } 
            //кнопка возврата в модуль
            echo('<a href="/' . $modul . '">Вернуться на страницу</a>');
            break;
    }
    if (isset($_POST['del_pic']))//
    {
        $id_pic = $_POST['id_pic'];
        $name = $_POST['name_pic'];
        mysqli_query($link, "DELETE FROM {$modul} WHERE id_pic={$id_pic}");
        mysqli_query($link, "DELETE FROM Picture WHERE id={$id_pic}");
        unlink($name);
        echo('<a href="/' . $modul . '">Вернуться на страницу</a>');
    }

}
//закрываем коннект
mysqli_close($link);
?>
